-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17-Jun-2019 às 15:48
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `livraria`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_autores`
--

CREATE TABLE `liv_autores` (
  `id_autor` int(11) NOT NULL,
  `autor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `liv_autores`
--

INSERT INTO `liv_autores` (`id_autor`, `autor`) VALUES
(12, 'Carlos Drummond de Andrade'),
(16, 'Guimarães Rosa'),
(17, 'Paulo Coelho');

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_editoras`
--

CREATE TABLE `liv_editoras` (
  `id_editora` int(11) NOT NULL,
  `editora` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `liv_editoras`
--

INSERT INTO `liv_editoras` (`id_editora`, `editora`) VALUES
(1, 'Abril'),
(2, 'Galápagos'),
(4, 'Novo Tempo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_livros`
--

CREATE TABLE `liv_livros` (
  `id_livro` int(11) NOT NULL,
  `id_editora` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `valor` float(3,2) NOT NULL,
  `capa` varchar(50) NOT NULL,
  `f1` varchar(50) DEFAULT NULL,
  `f2` varchar(50) DEFAULT NULL,
  `f3` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `liv_livros`
--

INSERT INTO `liv_livros` (`id_livro`, `id_editora`, `id_autor`, `titulo`, `valor`, `capa`, `f1`, `f2`, `f3`) VALUES
(30, 1, 16, 'A noite de sábado', 9.99, 'capa dura', NULL, '30_2.png', NULL),
(31, 2, 12, 'O dia que não terminou', 9.99, 'Simples', '31_1.png', '31_2.png', '31_3.jpg'),
(32, 4, 17, 'Só começou', 9.99, 'Simples', '32_1.png', '32_2.png', '32_3.jpg'),
(33, 1, 12, 'O grande Rei', 9.99, 'Especial de Aniversário', '33_1.jpg', '33_2.jpg', '33_3.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `liv_usuarios`
--

CREATE TABLE `liv_usuarios` (
  `id_usu` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(62) NOT NULL,
  `nivel_acesso` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 para padrão, 1 admin, 2 técnico',
  `avatar` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `liv_usuarios`
--

INSERT INTO `liv_usuarios` (`id_usu`, `nome`, `email`, `senha`, `nivel_acesso`, `avatar`) VALUES
(1, 'Admin', 'teste@t.com', '$2y$10$on7NxJCamRjkboEJK2Nate932DkDfoNkpbSz7w.OH38suq1f8L3n2', 2, '1.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `liv_autores`
--
ALTER TABLE `liv_autores`
  ADD PRIMARY KEY (`id_autor`);

--
-- Indexes for table `liv_editoras`
--
ALTER TABLE `liv_editoras`
  ADD PRIMARY KEY (`id_editora`);

--
-- Indexes for table `liv_livros`
--
ALTER TABLE `liv_livros`
  ADD PRIMARY KEY (`id_livro`),
  ADD KEY `fkey_smrgicupap` (`id_editora`),
  ADD KEY `fkey_iikgmedlsr` (`id_autor`);

--
-- Indexes for table `liv_usuarios`
--
ALTER TABLE `liv_usuarios`
  ADD PRIMARY KEY (`id_usu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `liv_autores`
--
ALTER TABLE `liv_autores`
  MODIFY `id_autor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `liv_editoras`
--
ALTER TABLE `liv_editoras`
  MODIFY `id_editora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `liv_livros`
--
ALTER TABLE `liv_livros`
  MODIFY `id_livro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `liv_usuarios`
--
ALTER TABLE `liv_usuarios`
  MODIFY `id_usu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `liv_livros`
--
ALTER TABLE `liv_livros`
  ADD CONSTRAINT `fkey_iikgmedlsr` FOREIGN KEY (`id_autor`) REFERENCES `liv_autores` (`id_autor`),
  ADD CONSTRAINT `fkey_smrgicupap` FOREIGN KEY (`id_editora`) REFERENCES `liv_editoras` (`id_editora`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
