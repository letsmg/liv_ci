Projeto de demonstração de Sistema para Livraria

Para fazer download deste conteúdo pode usar o botão download neste link: 

https://gitlab.com/letsmg/liv_ci.git

ou clonar via git com o comando: 

git clone https://gitlab.com/letsmg/liv_ci.git
(para se manter com as últimas alterações, usar o comando "git pull".)
**Favor dar preferência a essa opção**

ou baixar no meu site:

https://www.manualdoesperto.com.br/liv_lara.zip 
(esse já está com todas dependências instaladas, mas atualizo com menos frequência)


O arquivo de banco de dados é o "banco.sql" que deve ser importado no seu banco de dados e executado para o funcionamento correto sendo 
compatível com banco MariaDB ou MySql.

Caso esteja utilizado a configuração padrão do MariaDB ou Mysql, com usuário "root" e "sem senha cadastrada", funcionará normalmente.
Mas se utilizam uma configuração diferente, podem alterá-la em:

"application/config/database.php"

O nome do banco dado foi "livraria", deve ser mantido para que não haja erro 
no teste.

O usuário de acesso ao painel administrativo "administrador" cadastrado é:
email: teste@t.com
senha: 123

________________________xxxxxxxxxxxxxxxxxxxxxxxx___________________________

Este sistema foi criado utilizado o framework Codeigniter, versão 3.1.1.0, o qual utiliza o padrão MVC.

Para sua criação utilizei além do HTML5, CSS3 e JQuery:
Windows 10, Xampp (MariaDB, Apache), VsCode, Bootstrap 4 com SASS, Composer, Yarn, FontAwesome 4, DBNinja e PHPMyAdmin e versionamento Git via HTTP ou SSH.  

Tenho minha biblioteca pessoal de arquivos CSS que uso em todos projetos para não precisar 
refazer sempre configurações que não tem no Bootstrap 4.

Também fiz adições ao Codeigniter para utilizar o "Laravel mix" que permite minimizar e converter os arquivos SCSS para CSS. Uso também retornos Json de requisições ajax feitas para usar o sistema de proteçao contra CSRF e XSS disponibilizado pelo framework. Por padrão também o Codeigniter já protege contra ataques de Injeção SQL. Ainda, quanto a proteção, utilizo senhas gravadas
no banco com criptografia "BCRYPT", que são mais seguras que MD5. É um tipo de 
hash (criptografia de mão única), que nunca gera a mesma chave, mesmo que a mesma senha seja utilizada por outro usuário.

Utilizei sessões do PHP para guardar por determinado tempo os dados do usuário
logado. Após 10 minutos sem interação com o site, ele faz logoff do usuário automaticamente. Esse tempo pode ser regulado no arquivo "config/config.php" 
alterando o valor de:
$config['sess_expiration'] = 600; #tempo em segundos

Note também que não apenas no logoff do usuário as páginas ao qual ele tinha acesso, expiraram, mas também no login, bloque-ei a visualização da home page, para que o usuário não esqueceça de fazer logoff.

Utilizei também uma funcionalidade do Bootstrap chamada "Modal" que serve para 
destacar algo numa "light box", espécie de "alert" do JS, que exibe um conteúdo 
num bloco suspenso na página. Nesse caso, ao cliar no botão "Detalhes" na home 
page.

Costumo baixar as bibliotecas front-end pelo Yarn para ficarem na pasta node_modules e as de back-end pelo composer para ficar na pasta vendor.
Mas como estou enviando o projeto compilado, os arquivos CSS e JS estão em
suas respectivas pastas. 

Também utilio Filezilla para subir arquivos grandes compactados e descompactá-los via ssh no servidor e ferramentas diversas como Mysql Workbench
ou Fly Speed Query para "comandos SQL" complexos.

Também já criei aplicativos espelho de sites para android através das webviews, embora não seja necessário pelo site ser responsivo, é interessante,
pois sendo um aplicativo é possível divulgar o site na PlayStore e criar um atalho facilmente na tela do smartphone.

__________________________xxxxxxxxxxxxxxxxxxxxxxxx______________________________