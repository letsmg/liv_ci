<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main class="container py-50">

    <section class="row mt-100 h-800">

        <div class="col-sm-6 offset-sm-3">

            <div class="card sombra  mt-3">
                <div class="card-header bg-warning">
                    <h5 class="card-title">Edição de dados de Editora</h5>
                </div>
                <div class="card-body">                    
                    <form id='editaeditora' class='form-horizontal'>

                        <input type="hidden" name="csrf_test_name" value="<?= $this->security->get_csrf_hash(); ?>" />
                        <input type="hidden" name="codigo" value="<?= $detalhes->id_editora ?>" />


                        <div class='form-group'>
                            <label for='nome'></label>
                            <input type='text' id='nome' name='nome' class='form-control' required
                                value='<?= $detalhes->editora ?>'>
                            <div class='text-muted small'>Nome</div>
                        </div>



                        <div class='form-group'>
                            <div id='retorno' class='alert alert-success invisible'></div>
                            <button class='btn btn-tema' type='submit'>
                                <span class='fa fa-save'></span>
                                Salvar
                            </button>

                            <a href="../lista" class='btn btn-secondary'>
                                <span class="fa fa-arrow-leclicado"></span>
                                Voltar
                            </a>
                        </div>
                    </form>

                    <div id='ret_alt'></div>

                </div>
            </div>

        </div>


    </section>
</main>


<script>
    $(document).ready(function(){

        $('#editaeditora').on('submit',function(e){
            var formdata = new FormData($("#editaeditora")[0]);
            if(e.isDefaultPrevented()) {
                /*NAO PRECISA FAZER NADA QUE JA DA O AVISO, o codigo abaixo nao funfa aqui
                * $('html, body').animate({ scrollTop: 0 }, 500);
                * */
            } else {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: "<?= base_url('editoras/edita'); ?>",
                    data: formdata,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function(data){
                        console.log(data);
                        $("input[name='csrf_test_name']").val(data.csrf);
                        $('#ret_alt').hide();
                        $('#ret_alt').html(data.msg);
                        $('#ret_alt').show(400);
                    },
                    error: function( data ){
                        if(!data.responseJSON){

                        //********************************
                        //AQUI DEVE SER INFORMADO ERRO NUM ARQUIVO DE LOG
                        //DEIXEI DESSA FORMA PARA TESTAR MAIS RAPIDO
                        //********************************

                            console.log(data.responseText);
                            $('#ret_alt').show(400).html(data.responseText);
                        }else{
                            $('#ret_alt').html('');
                            $.each(data.responseJSON.errors, function (key, value) {
                                //$('#ret_alt').show(400).append(key+': '+value+'<br>');
                                //console.log(key);
                                $('#ret_alt').show(400).append(value+'<br>');
                            });
                        }
                    }
                });
            }
        });

    });
</script>