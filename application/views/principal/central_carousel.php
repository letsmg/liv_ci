<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- inicio seção central 2 -->
<section id='central_carousel' class='row'>

<div class="col-12 text-left">
<h5>Nossos livros</h5>
</div>
<hr>

<?php  
    foreach ($livros as $livro) {
?>
<div class="col-sm-6 col-md-4 col-lg-3 sem-sub-grupo">
    <div class="card">
        
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            
            <?php
              if(!empty($livro->f1)){  
            ?>            
              <div class="carousel-item active">
                <img class="d-block w-100" src="<?= base_url('img/livros/'.$livro->id_livro.'/'.$livro->f1) ?>" alt="<?= $livro->f1 ?>">
              </div>





            <?php
              }
              if(!empty($livro->f2)){  
                if(empty($livro->f1)){ //se não houver foto 1 tem de usar a classe active aqui
            ?>              
              <div class="carousel-item active">
              <?php
                }else{
              ?>
              <div class="carousel-item">
              <?php
                }
              ?>
                <img class="d-block w-100" src="<?= base_url('img/livros/'.$livro->id_livro.'/'.$livro->f2) ?>" alt="<?= $livro->f2 ?>">
              </div>






            <?php
              }            
              if(!empty($livro->f3)){  
                if(empty($livro->f2) && empty($livro->f3)){ //se não houver f1 e f2 tem de usar a classe active aqui
            ?>              
                  <div class="carousel-item active">
            <?php
                }else{
            ?>
                  <div class="carousel-item">
              <?php
                }
              ?>
                <img class="d-block w-100" src="<?= base_url('img/livros/'.$livro->id_livro.'/'.$livro->f3) ?>" alt="<?= $livro->f3 ?>">
              </div>
            <?php
              }
            ?> 
                       
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        </div>


        <div class="card-body">
            <h5 class="card-title"><?= $livro->titulo ?></h5>
            <p class="card-text text-muted"><?= $livro->autor ?></p>
            <p class="card-text text-muted">Valor <?= $livro->valor ?></p>
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal<?= $livro->id_livro ?>">
                Detalhes
            </button>
        </div>
    </div>                            
</div>    


<!-- Modal -->
<div class="modal fade" id="modal<?= $livro->id_livro ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?= $livro->titulo ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="<?= base_url('img/livros/'.$livro->id_livro.'/'.$livro->f1); ?>" alt="<?= $livro->f1 ?>"
            title='<?= $livro->f1 ?>' class='img-fluid rounded'>
        
        <br>
        Autor: <?= $livro->autor ?>
        <br>
        Editora: <?= $livro->editora ?>
        <br>
        Capa: <?= $livro->capa ?>
        <br>
        Valor: <?= $livro->valor ?>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>        
      </div>
    </div>
  </div>
</div>

<?php
    }
?>

</section>
<!-- final seção central carousel -->

