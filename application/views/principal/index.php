<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<main id='home' class='container text-18'>

    <div class="py-100 bg-white sombra px-3">


        <article id='banner' class="row mt-70" >

            <div class="col-sm-4 py-10 text-right">
                <h1>Livraria Babilônia</h1>
                <h3 class="text-muted">Para quem é ligado em leitura</h3>

                <div id="some" class="text-justify alert bg-warning" >
                    Essa DIV vai sumir daqui se visualizado num dispositivo de até 992 pixels de largura, 
                    ou se você redimensionar o seu navegador horizontalmente até esse tamanho.
                    Utilizei uma ferramenta CSS chamada "Media Queries". usada normalmente para não redenrizar 
                    imagens que deixariam o carregamento lento no celular.
                </div>
            </div>

            

            <div class="col-sm-8 py-10">
                <img src="<?= base_url('img/f1.jpg'); ?>" alt="imagem1"
                title='imagem1' class='img-fluid'>
            </div>

        </article>

        <div class="col-12 my-5">
            <hr>
        </div>


        <div id="conteudo" class="mt-5">

            <?php    
                //carrega outra view com o estilo simples de exibição de imagem            
                $this->load->view('principal/central_simples.php');                
            ?>
            
            <div class="col-12 my-5">
                <hr>
            </div>

            <h4>Segundo tipo de exibição</h4>

            <?php    
                //carrega outra view com um segundo estilo de exibição de imagens            
                $this->load->view('principal/central_carousel.php');                
            ?>
            

        </div>

    </div>

    

</main>

