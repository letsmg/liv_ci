<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
        <footer class="bg-verde py-50 mt-auto">

            <div class="container">

                <div class="row sem-sub-branco sem-sub-branco text-white negrito">

                     <div class="col-sm-5 offset-sm-2">
                        <div class="row align-items-center">

                            <div class="col-8 sem-sub-branco text-right">
                                Livraria Babilônia®
                                <br>
                                Desenvolvido por <a href="https://www.facebook.com/luizeduardots" target='_blank' rel="noopener">
                                Luiz Eduardo                                
                                </a>
                                <br>
                                <a href="<?= base_url('/curriculo.pdf'); ?>">Currículo Atualizado</a> 
                            </div>

                            <div class="col-4">
                                <img src="<?= base_url('img/icones/logo.jpg'); ?>" alt=""
                                    title='' class='rounded' width='100px;'>
                            </div>
                        </div>

                    </div>

                    <div class="col-5">
                        <p>
                            <a href="https://www.facebook.com/Manual-do-Esperto-287297085225701/?modal=admin_todo_tour" target="_blank" rel="noopener" class="">
                                <img src="<?= base_url('img/icones/face.png'); ?>" alt="face" class='img-fluid rounded' width="30px">
                                Facebook<div class="fb-like" data-href="https://www.facebook.com/Manual-do-Esperto-287297085225701/?modal=admin_todo_tour" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                            </a>
                        </p>
                        <p><a href="https://www.instagram.com/manualdoesperto/" target="_blank" rel="noopener">
                            <img src="<?= base_url('img/icones/insta.jpg'); ?>" alt="face" class='img-fluid rounded' width="30px">
                            Instagram
                        </a></p>
                    </div>

                </div>

            </div>

        </footer>

		
        <!--Start of Tawk.to Script-->
        <?php /*perde 3 pontos de velocidade com o chat ativado no google insights*/ ?>
        <?php
            if($_SERVER['HTTP_HOST'] !== 'localhost'){
        ?>
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5bf987c079ed6453ccaaf35b/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->
        <?php
            }

        ?>

    </body>
</html>