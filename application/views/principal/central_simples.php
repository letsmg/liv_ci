<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- inicio seção central_simples -->
<section id='central_simples' class='row'>

<div class="col-12 text-left">
<h5>Nossos livros</h5>
</div>
<hr>

<?php
    foreach ($livros as $livro) {
?>
<div class="col-sm-6 col-md-4 col-lg-3 sem-sub-grupo">
    <div class="card">
      <?php
        if (!empty($livro->f1)) {
      ?>
          <img class="card-img-top" src="<?= base_url('img/livros/'.$livro->id_livro.'/'.$livro->f1) ?>" alt="Card image cap">
      <?php
        } else {
      ?>
          <img class="card-img-top" src="<?= base_url('img/sem_foto.png') ?>" alt="Card image cap">
      <?php
        }
      ?>


        <div class="card-body">
            <h5 class="card-title"><?= $livro->titulo ?></h5>
            <p class="card-text text-muted"><?= $livro->autor ?></p>
            <p class="card-text text-muted">Valor <?= $livro->valor ?></p>
            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal<?= $livro->id_livro ?>">
                Detalhes
            </button>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal<?= $livro->id_livro ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?= $livro->titulo ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php
        if (!empty($livro->f1)) {
      ?>
          <img src="<?= base_url('img/livros/'.$livro->id_livro.'/'.$livro->f1); ?>" alt="<?= $livro->f1 ?>"
            title='<?= $livro->f1 ?>' class='img-fluid rounded'>
      <?php
        } else {
      ?>
          <img src="<?= base_url('img/sem_foto.png'); ?>" alt="<?= $livro->f1 ?>"
            title='<?= $livro->f1 ?>' class='img-fluid rounded'>
      <?php
        }
      ?>

        <br>
        Autor: <?= $livro->autor ?>
        <br>
        Editora: <?= $livro->editora ?>
        <br>
        Capa: <?= $livro->capa ?>
        <br>
        Valor: <?= $livro->valor ?>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>


<?php
    }
?>

</section>
<!-- final seção central 1 -->