<?php defined('BASEPATH') OR exit('No direct script access allowed');
    /*dica do hostgator para aumentar velocidade codigo abaixo, comentar no local
    nao precisa mais usar por causa do arquivo config ja tem essa opção
    e também aumentou a pontuação no website.grader.com/*/

    // if (substr_count($_SERVER[‘HTTP_ACCEPT_ENCODING’], ‘gzip’)) ob_start(“ob_gzhandler”); else ob_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="pt-br">
    <meta name="author" content="Luiz Eduardo">
    <link rel="icon" href='<?= base_url('img/icones/logo.jpg'); ?>'>
    <?php
        //se houver cadastro no banco de dados de detalhes para SEO.. títulos, keywords, ele já puxa
        //para não precisar ter um cabeçalho diferente para cada página
        if(isset($detalhes->tag_title)){
    ?>
        <title><?= $detalhes->tag_title  ?> | Livraria Babilônia</title>
        <meta name="title" content ="<?= $detalhes->tag_title  ?> | Livraria Babilônia" >
        <?php /*<meta name="description" CONTENT="<?= substr($detalhes->secao,0,152) ?>">*/ ?>
        <meta name="description" CONTENT="<?= $detalhes->tag_description ?>">
        <meta property="og:title"         content="<?= $detalhes->tag_title ?> | Livraria Babilônia" />
        <?php /*<meta property="og:description"   content="<?= substr($detalhes->secao,0,152) ?>" />*/ ?>
        <meta property="og:description"   content="<?= $detalhes->tag_description ?>" />
    <?php
        }else{
    ?>
        <title>Livraria Babilônia, aprenda marketing digital e como ganhar dinheiro</title>
        <meta name="title" content ="Livraria Babilônia! as keywords vão aqui" >
        <meta name="description" CONTENT="A livraria que você compra, vende e troca seus livros quando quiser.">
        <meta property="og:title"         content="Livraria Babilônia! as keywords vão aqui" />
        <meta property="og:description"   content="A livraria que você compra, vende e troca seus livros quando quiser." />
    <?php
        }
    ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
        if(isset($_SESSION['codigo'])){ 
        //se estiver logado nao deve ser indexado nem seguido
    ?>
        <meta name="robots" content="noindex, nofollow">
    <?php
        }else{
    ?>
        <meta name="robots" content="index, follow">
    <?php
        }
    ?>
    <meta property="og:site_name"     content="Livraria Babilônia">
    <?php /*tem de ser current_url em baixo senao nao compartilha imagem corretamente*/ ?>
    <meta property="og:url"           content="<?= current_url() ?>" />
    <meta property="og:locale" content="pt_BR">
    <meta property="og:type"          content="website" />

    <meta property="fb:admins" content="codigo facebook"/>
    <meta property="fb:pages" content="codigo facebook">
    <?php
        if(isset($detalhes->img1)){
    ?>
    <?php /*essa meta tag é pra colocar a imagem correta quando compartilhar no face*/ ?>
        <meta property="og:image"         content="<?= base_url('img/posts/p/'.$detalhes->id_post.'/'.$detalhes->img1); ?>" />
    <?php
        }else{
    ?>
        <meta property="og:image"         content="<?= base_url('img/icones/logo.jpg'); ?>" />
    <?php
        }
    ?>

    <link rel="amphtml" href="https://www.viaderegra.com/livraria">
    <link rel="dns-prefetch" href="https://www.viaderegra.com.br/livraria">
    <link rel='dns-prefetch' href='https://www.facebook.com' />
    <link rel='dns-prefetch' href='https://www.youtube.com' />
    

    <script src="<?= base_url('js/jquery.min.js'); ?>" type='text/javascript'></script>
    <script src="<?= base_url('js/bootstrap.min.js'); ?>" type='text/javascript'></script>
    
    <?= link_tag(base_url('css/bootstrap.min.css')); ?>
    <?= link_tag(base_url('css/font-awesome.min.css')); ?>

    <?= link_tag(base_url('css/diversos.css')); ?>
    <?= link_tag(base_url('css/backgrounds.css')); ?>
    <?= link_tag(base_url('css/cores.css')); ?>    
    <?= link_tag(base_url('css/margens.css')); ?>
    <?= link_tag(base_url('css/fontes.css')); ?>
    <?= link_tag(base_url('css/mediaqueries.css')); ?>
   

<?php
    /* esse script do google adsense come uns 15 pontos de velocidade
    o codigo de compartilhar do face diminui 32 pontos de velocidade e o codigo
    */
    /*if($_SERVER['HTTP_HOST'] !== 'localhost'){
?>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-9023702883294632",
        enable_page_level_ads: true
    });
    </script>
<?php
    }*/
?>




</head>


<!-- oncontextmenu="return false" ondragstart="return false" onselectstart="return false" -->

<body class='d-flex flex-column' >