<div class="container">
    <div class="row py-150">

        <div class="col-sm-2 text-center">
            
            <a href="<?= base_url('usuarios/novo'); ?>" class='btn btn-primary'>
                <span class="fa fa-plus"></span>
                Novo
            </a>

            
            <input type="hidden" name="csrf_test_name" value="<?= $this->security->get_csrf_hash(); ?>" />

            <div id="ret_usuario" class="py-1"></div>
        </div>



        <div class="col-sm-9 offset-sm-1">

            <table class="table table-striped table-responsive">

                <thead>
                    <tr>
                        <th class="table-dark">Nome</th>
                        <th class="table-dark">Nível</th>
                        <th class="table-dark" colspan='5'>E-mail</th>                        
                    </tr>
                </thead>
                <tbody>
            <?php
                if(empty($usuarios)){
            ?>
                    <td colspan='5'>Nenhum usuario cadastrado ainda.</td>
            <?php
                }else{
                    foreach ($usuarios as $usuario){
            ?>
                    <tr>
                        <td><?= $usuario->nome ?></td>
                        <td><?= ($usuario->nivel==1?"Administrador":"Padrão") ?></td>
                        <td><?= $usuario->email ?></td>                        
                        <td>
                            <a href="<?= base_url('usuarios/edicao/'.$usuario->id_usu); ?>" class="btn btn-warning">
                                <span class='fa fa-edit'></span>
                                Editar
                            </a>
                        </td>
                        <td>
                            <button value="<?= $usuario->id_usu ?>" class='btn btn-danger excluir'>
                                <span class='fa fa-remove'></span>
                                Excluir
                            </button>
                        </td>
                        
                        <td>
                            <?php
                                if ($usuario->ativo == 1) {
                            ?>
                                <button value="<?= $usuario->id_usu ?>" class='btn btn-dark bloq'>
                                    <span class='fa fa-close'></span>
                                    Bloqueia
                                </button>
                            <?php
                                } else {
                            ?>
                                <button value='<?= $usuario->id_usu ?>' class='btn btn-primary desbloq'>
                                    <span class='fa fa-check'></span>
                                    Desbloqueia
                                </button>
                            <?php                                
                                }                
                            ?>
                            
                        </td>
                    </tr>
            <?php
                    }
                }
            ?>
                </tbody>
                <tfoot></tfoot>
            </table>
            
        </div>


        
    </div>
</div>


<script>

    
        $('.bloq').on('click',function(e){
            e.preventDefault();
            $.ajax({
                url: '<?= base_url('usuarios/bloq_usu'); ?>',                                                
                data: 'codigo='+$(this).val()+'&csrf_test_name='+$('[name=csrf_test_name]').val(),
                type: 'post',
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    $('#ret_usuario').hide(400);                    
                    $('#ret_usuario').show(400).html(data.msg);                    
                    $("input[name='csrf_test_name']" ).val(data.csrf);
                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                },
                error: function( data ){
                    if(!data.responseJSON){
                        console.log(data.responseText);
                        $('#ret_usuario').show(400).html(data.responseText);
                    }else{
                        $('#ret_usuario').html('');
                        $.each(data.responseJSON.errors, function (key, value) {
                            //$('#ret_usuario').show(400).append(key+': '+value+'<br>');
                            //console.log(key);
                            $('#ret_usuario').show(400).append(value+'<br>');
                        });
                    }
                }
            });
        });


        $('.desbloq').on('click',function(e){            
           $.ajax({
                url: '<?= base_url('usuarios/desbloq_usu'); ?>',                
                data: 'codigo='+$(this).val()+'&csrf_test_name='+$('[name=csrf_test_name]').val(),
                type: 'post',
                dataType: 'json',
                success: function(data){
                    console.log(data);
                    $('#ret_usuario').hide(400);                    
                    $('#ret_usuario').show(400).html(data.msg);                    
                    $("input[name='csrf_test_name']" ).val(data.csrf);
                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                },
                error: function( data ){
                    if(!data.responseJSON){
                        console.log(data.responseText);
                        $('#ret_usuario').show(400).html(data.responseText);
                    }else{
                        console.log(data.responseText);
                        $('#ret_usuario').html('');
                        $.each(data.responseJSON.errors, function (key, value) {
                            //$('#ret_usuario').show(400).append(key+': '+value+'<br>');
                            //console.log(key);
                            $('#ret_usuario').show(400).append(value+'<br>');
                        });
                    }
                }
            });
        });


    
    

    $('.excluir').on('click', function (e){
        var confirma = confirm("Tem certeza?");        
        if(confirma){
            if(e.isDefaultPrevented()){
                /*NAO PRECISA FAZER NADA QUE JA DA O AVISO, o codigo abaixo nao funfa aqui
                 * $('html, body').animate({ scrollTop: 0 }, 500);
                 * */
            }else{               
                e.preventDefault();
                $.ajax({
                    url: '<?= base_url('usuarios/excluir'); ?>',
                    type: 'post',
                    data: 'codigo='+$(this).val()+'&csrf_test_name='+$('[name=csrf_test_name]').val(),
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        $('[name=csrf_test_name]').val(data.csrf);
                        $('#ret_usuario').hide('fast');
                        $('#ret_usuario').html(data.msg);
                        $('#ret_usuario').show('fast');
                        $('html, body').animate({scrollTop: 0}, 500);
                        if(data.extra !== '2'){
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        }
                    }
                });
            }
        }
    });

</script>