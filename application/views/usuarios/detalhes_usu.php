<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main class="container py-50">
    <section class="row">

        <div class="col-sm-8 offset-sm-2 col-lg-6 offset-lg-3 mt-100">

            <div class="card sombra">
                <div class="card-body">
                
                    
                    <form id='cadusu' class='form-horizontal'>

                        <input type="hidden" name="csrf_test_name" value="<?= $this->security->get_csrf_hash(); ?>" />
                        <input type="hidden" name="codigo" value="<?= $detalhes->id_usu ?>" />

                        <div class='form-group'>
                            <label for='nome'></label>
                            <input type='text' id='nome' name='nome' class='form-control' 
                                required value='<?= $detalhes->nome ?>'>
                            <div class='text-muted small'>Nome</div>
                        </div>

                        <div class='form-group'>
                            <label for='nick'></label>
                            <input type='text' id='nick' name='nick' class='form-control'
                               required maxlength='30' value='<?= $detalhes->nick ?>'>
                            <div class='text-muted small'>Nick (apelido curto para os leitores verem)</div>
                        </div>

                        <div class='form-group'>
                            <label for='email'></label>
                            <input type='email' id='email' name='email' class='form-control' 
                                required value='<?= $detalhes->email ?>'>
                            <div class='text-muted small'>E-mail</div>
                        </div>

                        <div class='form-group'>
                            <label for='senha'></label>
                            <input type='password' id='senha' name='senha' class='form-control' 
                                required >
                            <div class='text-muted small'>Senha</div>
                        </div>

                        <div class='form-group'>
                            <select id='nivel' name='nivel' class="custom-select" required>                                
                                <option value="<?= $detalhes->nivel ?>" >
                                    <?= ($detalhes->nivel==1)?"Administrador":($detalhes->nivel==2)?"CEO":"Padrão" ?>
                                </option>                                
                                <option value="0" >
                                    Padrão
                                </option>                                
                                <?php
                                    if($detalhes->nivel==2 or $detalhes->nivel==1 ){
                                ?>
                                <option value="1" >
                                    Administrador
                                </option> 
                                <?php
                                    }
                                    if($detalhes->nivel==2){
                                ?>
                                    <option value="2" >
                                        CEO
                                    </option> 
                                <?php
                                    }
                                ?>
                                </div>
                            </select>
                        </div>

                        <div class='form-group'>
                            <div id='retorno' class='alert alert-success invisible'></div>
                            <button class='btn btn-tema' type='submit'>
                                <span class='fa fa-save'></span>
                                Salvar
                            </button>
                            <a href="../" class='btn btn-secondary'>
                                <span class="fa fa-arrow-circle-left"></span>
                                Voltar
                            </a>
                        </div>
                    </form>

                    <div id='ret_cad'></div>
                
                </div>
            </div>

        </div>

        

    </section>
</main>


<script>
    $(document).ready(function(){
        $('#cadusu').on('submit',function(e){
            if(e.isDefaultPrevented()) {
                /*NAO PRECISA FAZER NADA QUE JA DA O AVISO, o codigo abaixo nao funfa aqui
                * $('html, body').animate({ scrollTop: 0 }, 500);
                * */
            } else {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: "<?= base_url('usuarios/edita'); ?>",
                    data: $('#cadusu').serialize(),
                    dataType: 'json',
                    success: function(data){
                        //console.log(msg);
                        $('#ret_cad').hide(400);                        
                        $('#ret_cad').html(data.msg);    
                        $('#ret_cad').show(400);
                        
                        $("input[name='csrf_test_name']").val(data.csrf);
                    }
                });
            }
        });
    });
</script>