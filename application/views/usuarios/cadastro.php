<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main class="container py-50">
    <section class="row">

        <div class="col-sm-8 offset-sm-2 col-lg-6 offset-lg-3 mt-100">

            <div class="card sombra">
                <div class="card-body">
                                    
                    </span>
                    <form id='cadusu' class='form-horizontal'>

                        <input type="hidden" name="csrf_test_name" value="<?= $this->security->get_csrf_hash(); ?>" />

                        <div class='form-group'>
                            <label for='nome'></label>
                            <input type='text' id='nome' name='nome' class='form-control' required>
                            <div class='text-muted small'>Nome</div>
                        </div>

                        <div class='form-group'>
                            <label for='email'></label>
                            <input type='email' id='email' name='email' class='form-control' required>
                            <div class='text-muted small'>E-mail</div>
                        </div>

                        <div class='form-group'>
                            <label for='senha'></label>
                            <input type='password' id='senha' name='senha' class='form-control' required>
                            <div class='text-muted small'>Senha</div>
                        </div>

                        <div class='form-group'>
                            <select id='nivel' name='nivel' class="custom-select" required>
                                <option value='' selected>Escolha um nível de acesso</option>                            
                                    <option value="0" >
                                        Padrão
                                    </option>                                
                                    <option value="1" >
                                        Administrador
                                    </option> 
                                </div>
                            </select>
                        </div>

                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="img" name="img">
                                <label class="custom-file-label" for="img">Procurar arquivo</label>
                            </div>
                        </div>

                        <div class='form-group'>
                            <div id='retorno' class='alert alert-success invisible'></div>
                            <button class='btn btn-tema' type='submit'>
                                <span class='fa fa-send'></span>
                                Enviar
                            </button>
                        </div>
                    </form>

                    <div id='ret_cad'></div>
                
                </div>
            </div>

        </div>

        

    </section>
</main>


<script>
    $(document).ready(function(){
        $('#cadusu').on('submit',function(e){
            var formdata = new FormData($("#cadusu")[0]);
            if(e.isDefaultPrevented()) {
                /*NAO PRECISA FAZER NADA QUE JA DA O AVISO, o codigo abaixo nao funfa aqui
                * $('html, body').animate({ scrollTop: 0 }, 500);
                * */
            } else {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: "<?= base_url('usuarios/cadastra'); ?>",
                    data: formdata,
                    dataType: 'json',                    
                    processData: false,
                    contentType: false,
                    success: function(data){
                        //console.log(msg);
                        $('#ret_cad').html(data.msg);
                        $('#ret_cad').append(data.img);
                        $("input[name='csrf_test_name']").val(data.csrf);
                    },
                    error: function( data ){
                        if(!data.responseJSON){
                            console.log(data.responseText);
                            $('#ret_cad').show(400).html(data.responseText);
                        }else{
                            $('#ret_cad').html('');
                            $.each(data.responseJSON.errors, function (key, value) {
                                //$('#ret_cad').show(400).append(key+': '+value+'<br>');
                                //console.log(key);
                                $('#ret_cad').show(400).append(value+'<br>');
                            });
                        }
                    }
                });
            }
        });
    });
</script>

<?php
    if($_SERVER['SERVER_NAME'] == 'localhost'){
            //echo _SERVER['SERVER_NAME'];    
    ?>
    <!--  *********************************************** 
    script para testes 
    -->
    <script>
        $(document).ready(function(){
            $('#preenche').on('click',function(e){
                e.preventDefault();
                //var inputs = new Array();            
                $('input').each(function(){            
                    if($(this).attr('name') !== 'csrf_test_name' && $(this).attr('type') !== 'file'){
                    //inputs.push($(this).val());
                    min = Math.ceil(0);
                    max = Math.floor(99);
                    $("input[name="+$(this).attr('name')+"]").val($(this).attr('name')+'_'+Math.floor(Math.random() * (max - min)));
                    }
                });
                //$('input[type=text]').val($(this).attr('name')); //deixa todos iguais
                $('input[type=date]').val('1985-12-06');
                $('input[type=email]').val('teste@teste.com');
                //$('option[value=MG]').attr('selected','selected');
                $("select").prop("selectedIndex", 2);
                $('input[type=checkbox]').attr('checked','checked');
                $('textarea').each(function(){
                   if($(this).attr('name') !== 'csrf_test_name' && $(this).attr('type') !== 'file'){
                   //inputs.push($(this).val());
                   $("textarea[name="+$(this).attr('name')+"]").val($(this).attr('name'));
                   }
               });
            });
        });
    </script>

    <div class='container bg-white text-center'>
        <a href="#" id="preenche" class="btn btn-success">
            <span class="fa fa-plus"></span>
            Preencher inputs
        </a>
    </div>
    <!--  *********************************************** 
        script para testes 
    -->
    <?php
    }
?>