<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main class="container py-50">
    <section class="row mt-100 h-800">

        <div class="col-sm-6">

            <div class="card sombra  mt-3">
                
                <div class="card-header bg-success text-white">
                    <h5 class="card-title">Cadastre quantos autores quiser</h5>
                </div>
                
                <div class="card-body">
                    
                    <form id='cadautor' class='form-horizontal'>

                        <input type="hidden" name="csrf_test_name" value="<?= $this->security->get_csrf_hash(); ?>" />

                        <div class='form-group'>
                            <label for='nome'></label>
                            <input type='text' id='nome' name='nome' class='form-control' required>
                            <div class='text-muted small'>Nome</div>
                        </div>



                        <div class='form-group'>
                            <div id='retorno' class='alert alert-success invisible'></div>
                            <button class='btn btn-tema' type='submit'>
                                <span class='fa fa-send'></span>
                                Enviar
                            </button>

                            <a href="lista" class='btn btn-secondary'>
                                <span class="fa fa-arrow-left">Voltar</span>
                                
                            </a>
                        </div>
                    </form>

                    <div id='ret_cad'></div>

                </div>
            </div>

        </div>

        <div class="col-sm-6 mt-3">
            <div class="card sombra">
                <div class="card-header bg-success text-white">
                    <h5 class="card-title">Veja os autores já cadastrados</h5>
                </div>

                <div class="card-body">
                    
                    <p class="text-muted">Caso deseje apagar algum autor, clique 
                        <a href="lista">aqui</a>
                    </p>
                    <div class='form-group'>
                        <select id='autores' name='autores' class="custom-select" >
                            <option selected>Escolha um autor...</option>                            
                                <?php
                                    foreach ($autores as $autor) {
                                ?>
                                        <option value="<?= $autor->id_autor ?>" >
                                            <?= $autor->autor ?>
                                        </option>
                                <?php
                                    }
                                ?>
                            </div>
                        </select>
                    </div>
                </div>
            </div>
            
        </div>

    </section>
</main>


<script>
    $(document).ready(function(){
        $('#cadautor').on('submit',function(e){
            var formdata = new FormData($("#cadautor")[0]);
            if(e.isDefaultPrevented()) {
                /*NAO PRECISA FAZER NADA QUE JA DA O AVISO, o codigo abaixo nao funfa aqui
                * $('html, body').animate({ scrollTop: 0 }, 500);
                * */
            } else {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: "<?= base_url('autores/cadastra'); ?>",
                    data: formdata,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function(data){
                        //console.log(data);
                        $("input[name='csrf_test_name']").val(data.csrf);                    
                        $('#ret_cad').hide();
                        $('#ret_cad').html(data.msg);                        
                        $('#ret_cad').show(400);
                    },
                    error: function( data ){
                        if(!data.responseJSON){

                            //********************************
                            //AQUI DEVE SER INFORMADO ERRO NUM ARQUIVO DE LOG
                            //DEIXEI DESSA FORMA PARA TESTAR MAIS RAPIDO
                            //********************************

                            console.log(data.responseText);
                            $('#ret_cad').show(400).html(data.responseText);
                        }else{
                            $('#ret_cad').html('');
                            $.each(data.responseJSON.errors, function (key, value) {
                                //$('#ret_cad').show(400).append(key+': '+value+'<br>');
                                //console.log(key);
                                $('#ret_cad').show(400).append(value+'<br>');
                            });
                        }
                    }
                });
            }
        });
    });
</script>