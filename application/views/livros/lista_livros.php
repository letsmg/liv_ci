<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    
    <div class="row py-150 sombra h-800">

        <div class="col-sm-4 offset-sm-1 text-center">
            
            <a href="<?= base_url('livros/novo'); ?>" class='btn btn-primary'>
                <span class="fa fa-plus"></span>
                Novo
            </a>

            <a href="<?= base_url(); ?>" class='btn btn-secondary'>
                <span class="fa fa-arrow-circle-left"></span>
                Home
            </a>
            
            <input type="hidden" name="csrf_test_name" value="<?= $this->security->get_csrf_hash(); ?>" />

            <div id="ret_usuario" class="py-1"></div>
        </div>


        <div class="col-sm-7 ">
            
            <table class="table table-striped table-responsive">

                <thead>
                    <tr>                    
                        <th class="table-dark" colspan='3'>Livros cadastrados</th>
                    </tr>
                </thead>
                <tbody>
            <?php
                if(empty($livros)){
            ?>
                    <td colspan='5'>Nenhum usuario cadastrado ainda.</td>
            <?php
                }else{
                    foreach ($livros as $livro){
            ?>
                    <tr>
                        <td><?= $livro->titulo ?></td>                        
                        <td>
                            <a href="<?= base_url('livros/edicao/'.$livro->id_livro); ?>" class="btn btn-warning">
                                <span class='fa fa-edit'></span>
                                Editar
                            </a>
                        </td>
                        <td>
                            <button value="<?= $livro->id_livro ?>" class='btn btn-danger excluir'>
                                <span class='fa fa-remove'></span>
                                Excluir
                            </button>
                        </td>
                        
                        
                    </tr>
            <?php
                    }
                }
            ?>
                </tbody>
                <tfoot></tfoot>
            </table>
            
        </div>
        
    </div>
</div>


<script>

    $('.excluir').on('click', function (e){
        var confirma = confirm("Tem certeza?");        
        if(confirma){
            if(e.isDefaultPrevented()){
                /*NAO PRECISA FAZER NADA QUE JA DA O AVISO, o codigo abaixo nao funfa aqui
                 * $('html, body').animate({ scrollTop: 0 }, 500);
                 * */
            }else{               
                e.preventDefault();
                $.ajax({
                    url: '<?= base_url('livros/excluir'); ?>',
                    type: 'post',
                    data: 'codigo='+$(this).val()+'&csrf_test_name='+$('[name=csrf_test_name]').val(),
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        $('[name=csrf_test_name]').val(data.csrf);
                        $('#ret_usuario').hide('fast');
                        $('#ret_usuario').html(data.msg+"\n <div class='alert alert-warning'>A página <b>será atualizada</b> em alguns segundos.</div>");
                        $('#ret_usuario').show('fast');
                        $('html, body').animate({scrollTop: 0}, 500);
                        if(data.extra !== '2'){
                            setTimeout(() => {
                                location.reload();
                            }, 4000);
                        }
                    }
                });
            }
        }
    });

</script>