<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main class="container py-50">
    <section class="row mt-100 h-800">

        <div class="col-sm-6 offset-sm-3 text-center alert bg-amarelo-dark text-white">
            <b>Atenção!</b> Você só consegue cadastrar um livro após cadastrar seu autor e 
            editora no menu de cadastros.
        </div>

        <div class="col-sm-6">

            <div class="card sombra mt-3">
                <div class="card-header bg-primary text-white">
                    <h5 class="card-title">Cadastre quantos livros quiser</h5>
                </div>

                <div class="card-body">

                    
                    
                    <form id='cadlivro' class='form-horizontal'>

                        <input type="hidden" name="csrf_test_name" value="<?= $this->security->get_csrf_hash(); ?>" />

                        <div class='form-group'>
                            <select id='autor' name='autor' class="custom-select" >
                                <option selected>Escolha um autor...</option>                            
                                <?php
                                    foreach ($autores as $autor) {
                                ?>
                                        <option value="<?= $autor->id_autor ?>" >
                                            <?= $autor->autor ?>
                                        </option>
                                <?php
                                    }
                                ?>
                                </div>
                            </select>
                            <div class='text-muted small'>Autor</div>
                        </div>

                        <div class='form-group'>
                            <select id='editora' name='editora' class="custom-select" >
                                <option selected>Escolha uma editora...</option>                            
                                <?php
                                    foreach ($editoras as $ed) {
                                ?>
                                        <option value="<?= $ed->id_editora ?>" >
                                            <?= $ed->editora ?>
                                        </option>
                                <?php
                                    }
                                ?>                                
                            </select>
                            <div class='text-muted small'>Editora</div>
                        </div>

                        <div class='form-group'>
                            <label for='titulo'></label>
                            <input type='text' id='titulo' name='titulo' class='form-control' required>
                            <div class='text-muted small'>titulo</div>
                        </div>

                        <div class='form-group'>
                            <label for='capa'></label>
                            <input type='text' id='capa' name='capa' class='form-control' required>
                            <div class='text-muted small'>capa</div>
                        </div>

                        <div class='form-group'>
                            <label for='valor'></label>
                            <input type='number' id='valor' name='valor' class='form-control'
                               required maxlength='30' >
                            <div class='text-muted small'>valor</div>
                        </div>

                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="img" name="img">
                                <label class="custom-file-label" for="img">Imagem 1 "Principal" (opcional)</label>
                            </div>
                        </div>

                        <br>

                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="img2" name="img2">
                                <label class="custom-file-label" for="img2">Imagem 2 (opcional)</label>
                            </div>
                        </div>

                        <br>

                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="img3" name="img3">
                                <label class="custom-file-label" for="img3">Imagem 3 (opcional)</label>
                            </div>
                        </div>

                        

                        <div class='form-group'>
                            <div id='retorno' class='alert alert-success invisible'></div>
                            <button class='btn btn-tema' type='submit'>
                                <span class='fa fa-send'></span>
                                Enviar
                            </button>

                            <a href="lista" class='btn btn-secondary'>
                                <span class="fa fa-arrow-left">Voltar</span>
                                
                            </a>
                        </div>
                    </form>

                    <div id='ret_cad'></div>

                </div>
            </div>

        </div>

        <div class="col-sm-6 mt-3">
            <div class="card sombra">
                <div class="card-header bg-primary text-white">
                    <h5 class="card-title">Veja os livros já cadastrados</h5>
                    Atualize teclando "F5" para ver os que cadastrou agora.
                </div>

                <div class="card-body">
                    
                    <p class="text-muted">Caso deseje apagar algum livro, clique 
                        <a href="lista">aqui</a>
                    </p>
                    <div class='form-group'>
                        <select id='livros' name='livros' class="custom-select" >
                            <option selected>Escolha um livro...</option>                            
                                <?php
                                    foreach ($livros as $livro) {
                                ?>
                                        <option value="<?= $livro->id_livro ?>" >
                                            <?= $livro->titulo ?>
                                        </option>
                                <?php
                                    }
                                ?>
                            </div>
                        </select>
                    </div>
                </div>
            </div>
            
        </div>

    </section>
</main>


<script>
    $(document).ready(function(){
        $('#cadlivro').on('submit',function(e){
            var formdata = new FormData($("#cadlivro")[0]);
            if(e.isDefaultPrevented()) {
                /*NAO PRECISA FAZER NADA QUE JA DA O AVISO, o codigo abaixo nao funfa aqui
                * $('html, body').animate({ scrollTop: 0 }, 500);
                * */
            } else {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: "<?= base_url('livros/cadastra'); ?>",
                    data: formdata,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function(data){
                        //console.log(data);
                        $("input[name='csrf_test_name']").val(data.csrf);                    
                        $('#ret_cad').hide();
                        $('#ret_cad').html(data.msg);                        
                        $('#ret_cad').show(400);
                    },
                    error: function( data ){ 
                        
                        //********************************
                        //AQUI DEVE SER INFORMADO ERRO NUM ARQUIVO DE LOG
                        //DEIXEI DESSA FORMA PARA TESTAR MAIS RAPIDO
                        //********************************

                        if(!data.responseJSON){
                            console.log(data.responseText);
                            $('#ret_cad').show(400).html(data.responseText);
                        }else{
                            $('#ret_cad').html('');
                            $.each(data.responseJSON.errors, function (key, value) {
                                //$('#ret_cad').show(400).append(key+': '+value+'<br>');
                                //console.log(key);
                                $('#ret_cad').show(400).append(value+'<br>');
                            });
                        }
                    }
                });
            }
        });
    });
</script>



<!-- ESTE TRECHO DO CÓDIGO ABAIXO É PARA FACILITAR TESTAR -->


<?php
    if($_SERVER['SERVER_NAME'] == 'localhost'){
            //echo _SERVER['SERVER_NAME'];    
    ?>
    <!--  *********************************************** 
    script para testes 
    -->
    <script>
        $(document).ready(function(){
            $('#preenche').on('click',function(e){
                e.preventDefault();
                //var inputs = new Array();            
                $('input').each(function(){            
                    if($(this).attr('name') !== 'csrf_test_name' && $(this).attr('type') !== 'file'){
                    //inputs.push($(this).val());
                    min = Math.ceil(0);
                    max = Math.floor(99);
                    $("input[name="+$(this).attr('name')+"]").val($(this).attr('name')+'_'+Math.floor(Math.random() * (max - min)));
                    }
                });
                //$('input[type=text]').val($(this).attr('name')); //deixa todos iguais
                $('input[type=date]').val('1985-12-06');
                $('input[type=email]').val('teste@teste.com');                
                $('input[type=number]').val(123);
                //$('option[value=MG]').attr('selected','selected');
                $("select").prop("selectedIndex", 2);
                $('input[type=checkbox]').attr('checked','checked');
                $('textarea').each(function(){
                   if($(this).attr('name') !== 'csrf_test_name' && $(this).attr('type') !== 'file'){
                   //inputs.push($(this).val());
                   $("textarea[name="+$(this).attr('name')+"]").val($(this).attr('name'));
                   }
               });
            });
        });
    </script>

    <div class='container bg-white text-center py-5'>
        <a href="#" id="preenche" class="btn btn-success">
            <span class="fa fa-plus"></span>
            Preencher inputs (para testar mais rápido, preenche inputs aleatoriamente)
        </a>
    </div>
    <!--  *********************************************** 
        script para testes 
    -->
    <?php
    }
?>