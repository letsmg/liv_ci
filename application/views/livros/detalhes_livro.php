<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main class="container py-50">

    <section class="row mt-100 h-800">

        <div class="col-sm-10 offset-sm-1">

            <div class="card sombra  mt-3">

                <div class="card-header bg-primary text-white">
                    <h5 class="card-title">Edição de dados de Livro</h5>
                </div>

                <div class="card-body">


                    <form id='editalivro' class='form-horizontal'>

                        <input type="hidden" name="csrf_test_name" value="<?= $this->security->get_csrf_hash(); ?>" />

                        <div class="form-group">
                            <input type="text" class='form-control' readonly id="codigo" name="codigo" value="<?= $detalhes->id_livro ?>" />
                            <div class='text-muted small'>Id</div>
                        </div>

                        <div class='form-group'>
                            <select id='autor' name='autor' class="custom-select" >
                                <option value="<?= $detalhes->id_autor ?>" selected><?= $detalhes->autor ?></option>                            
                                <?php
                                    foreach ($autores as $autor) {
                                ?>
                                        <option value="<?= $autor->id_autor ?>" >
                                            <?= $autor->autor ?>
                                        </option>
                                <?php
                                    }
                                ?>                                
                            </select>
                            <div class='text-muted small'>Autor</div>
                        </div>

                        <div class='form-group'>
                            <select id='editora' name='editora' class="custom-select" >
                                <option value="<?= $detalhes->id_editora ?>" selected><?= $detalhes->editora ?></option>                            
                                <?php
                                    foreach ($editoras as $ed) {
                                ?>
                                        <option value="<?= $ed->id_editora ?>" >
                                            <?= $ed->editora ?>
                                        </option>
                                <?php
                                    }
                                ?>                                
                            </select>
                            <div class='text-muted small'>Editora</div>
                        </div>

                        
                        <div class='form-group'>
                            <label for='titulo'></label>
                            <input type='text' id='titulo' name='titulo' class='form-control' required value='<?= $detalhes->titulo ?>'>
                            <div class='text-muted small'>titulo</div>
                        </div>

                        <div class='form-group'>
                            <label for='capa'></label>
                            <input type='text' id='capa' name='capa' class='form-control' required value='<?= $detalhes->capa ?>'>
                            <div class='text-muted small'>capa</div>
                        </div>

                        <div class='form-group'>
                            <label for='valor'></label>
                            <input type='number' id='valor' name='valor' class='form-control'
                               required maxlength='30'  value='<?= $detalhes->valor ?>'>
                            <div class='text-muted small'>valor</div>
                        </div>

                        <div class="row">
                        <div class="col-md-6 col-lg-4 mb-3">
                            <div class="card">                        
                                <img class="card-img-top" src="<?= base_url('img/livros/'.$detalhes->id_livro.'/'.$detalhes->f1); ?>" alt="Card image cap">
                                <div class="card-body bg-verde">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="img" name="img">
                                        <label class="custom-file-label" for="img">Imagem 1 "Principal" (opcional)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-lg-4 mb-3">
                            <div class="card">
                                <img class="card-img-top" src="<?= base_url('img/livros/'.$detalhes->id_livro.'/'.$detalhes->f2); ?>" alt="Card image cap">
                                <div class="card-body bg-danger">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="img2" name="img2">
                                        <label class="custom-file-label" for="img">Imagem 2 (opcional)</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-md-6 col-lg-4 mb-3">
                            <div class="card">
                                <img class="card-img-top" src="<?= base_url('img/livros/'.$detalhes->id_livro.'/'.$detalhes->f3); ?>" alt="Card image cap">
                                <div class="card-body bg-primary">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="img3" name="img3">
                                        <label class="custom-file-label" for="img">Imagem 3 (opcional)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class='form-group'>
                            <div id='retorno' class='alert alert-success invisible'></div>
                            <button class='btn btn-tema' type='submit'>
                                <span class='fa fa-save'></span>
                                Salvar
                            </button>

                            <a href="../lista" class='btn btn-secondary'>
                                <span class="fa fa-arrow-circle-left"></span>
                                Voltar
                            </a>
                            <br><br>
                            <div class="form-group apaga">
                                <a id='<?= $detalhes->f1 ?>' href="#" class='btn btn-danger'>
                                    <span class="fa fa-remove"></span>
                                    Apaga foto 1
                                </a>
                            </div>

                            <div class="form-group apaga">
                                <a id='<?= $detalhes->f2 ?>' href="#" class='btn btn-danger'>
                                    <span class="fa fa-remove"></span>
                                    Apaga foto 2
                                </a>
                            </div>

                            <div class="form-group apaga">
                                <a id='<?= $detalhes->f3 ?>' href="#" class='btn btn-danger'>
                                    <span class="fa fa-remove"></span>
                                    Apaga foto 3
                                </a>
                            </div>
                        </div>
                    </form>

                    <div id='ret_det'></div>

                </div>
            </div>

        </div>


    </section>
</main>


<script>
    $(document).ready(function(){

        $('#editalivro').on('submit',function(e){
            var formdata = new FormData($("#editalivro")[0]);
            if(e.isDefaultPrevented()) {
                /*NAO PRECISA FAZER NADA QUE JA DA O AVISO, o codigo abaixo nao funfa aqui
                * $('html, body').animate({ scrollTop: 0 }, 500);
                * */
            } else {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: "<?= base_url('livros/edita'); ?>",
                    data: formdata,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function(data){
                        //console.log(data);                        
                        $("input[name='csrf_test_name']").val(data.csrf);
                        $('#ret_det').hide();
                        $('#ret_det').html(data.msg+data.img);
                        $('#ret_det').show(400);
                    },
                    error: function( data ){
                        if(!data.responseJSON){
                            console.log(data.responseText);
                            $('#ret_det').show(400).html(data.responseText);
                        }else{
                            $('#ret_det').html('');
                            $.each(data.responseJSON.errors, function (key, value) {
                                //$('#ret_det').show(400).append(key+': '+value+'<br>');
                                //console.log(key);
                                $('#ret_det').show(400).append(value+'<br>');
                            });
                        }
                    }
                });
            }
        });


        $('.apaga a').on('click', function(evt){
            evt.preventDefault();
            var ft = $(this).attr('id');            
            var confirma = confirm('Tem certeza?');
            if(confirma){
                $.ajax({
                    url: '<?= base_url('livros/apagaft'); ?>',
                    data: 'ft='+ft+'&id='+<?= $detalhes->id_livro ?>+'&csrf_test_name='+$("input[name='csrf_test_name']" ).val(),
                    type: 'post',
                    dataType: 'json',
                    success: function(data){
                        console.log(data);
                        $('#ret_det').hide(400);
                        setTimeout(() => {
                            $('#ret_det').show(400).html(data.msg);
                        }, 700);
                        $("input[name='csrf_test_name']" ).val(data.csrf);
                    },error: function( data ){
                        if(!data.responseJSON){
                            console.log(data.responseText);
                            $('#ret_det').show(400).html(data.responseText);
                        }else{
                            $('#ret_det').html('');
                            $.each(data.responseJSON.errors, function (key, value) {
                                //$('#ret_det').show(400).append(key+': '+value+'<br>');
                                //console.log(key);
                                $('#ret_det').show(400).append(value+'<br>');
                            });
                        }
                    }
                });
            }
        });



    });
</script>