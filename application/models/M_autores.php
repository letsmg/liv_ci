<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class M_autores extends CI_Model {

        public function novo_autor() //salva novo autor no banco
        {
            $this->db->where('autor',$this->input->post('nome'));
            $ja_tem = $this->db->get('liv_autores')->row();
            //echo $this->db->get_compiled_select('liv_autores');

            // var_dump($ja_tem);
            // exit();
            if ($ja_tem) {
                return 2;
            }

            $dados = array(
               'autor' => $this->input->post('nome')
            );

            if($this->db->insert('liv_autores', $dados)){
                return 1;
            }else{
                return 3;
            }

        }

        public function lista_todos() //retorna todos autores
        {
            return $this->db->get('liv_autores')->result();
        }
        
        public function lista_detalhes($codigo) //detalhes de autor escolhido
        {
            $this->db->where('id_autor',$codigo);
            return $this->db->get('liv_autores')->row();
        }

        public function altera_autor()
        {
            $this->db->where('autor',$this->input->post('nome'));             
            $this->db->where('id_autor !=',$this->input->post('codigo'));
            $ja_tem = $this->db->count_all_results('liv_autores');
            
            //verifica se nome ja estiver cadastrado e não for esse mesmo
            if($ja_tem > 0){                
                return 2;                
            }

            $dados = array(
               'autor' => $this->input->post('nome')
            );
            
            $this->db->where('id_autor', $this->input->post('codigo'));
            return $this->db->update('liv_autores', $dados);
        }
        
        public function apaga_autor()
        {
            $this->db->where('id_autor', $this->input->post('codigo'));
            return $this->db->delete('liv_autores');
        }

    }

