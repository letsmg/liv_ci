<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class M_livros extends CI_Model {

        public function novo_livro() //salva novo livro no banco
        {
            $this->db->where('titulo',$this->input->post('titulo'));
            $ja_tem = $this->db->count_all_results('liv_livros');
            //echo $this->db->get_compiled_select('liv_livros');

            // var_dump($ja_tem);
            // exit();
            if ($ja_tem > 0) {
                return ['status' => 2];
            }

            $dados = array(
               'titulo' => $this->input->post('titulo'),
               'id_editora' => $this->input->post('editora'),
               'id_autor' => $this->input->post('autor'),
               'capa' => $this->input->post('capa'),
               'valor' => $this->input->post('valor'),
            );


            if($this->db->insert('liv_livros', $dados)){
                $id = $this->db->insert_id();

                //essa parte é necessaria para renomear os arquivos, facilitando
                //na hora de trocar uma foto por outra com o unlink, pois pode ser
                //que apenas uma das fotos seja alterada.
                foreach($_FILES as $f){
                    if(isset($f['name'])){
                        $partes = explode('.',$f['name']);
                        $ext[] = end($partes); //pois pode ter mais de 1 "." no nome
                    }
                }

                $dados = array(
                    'f1' => $id."_1.".$ext['0'],
                    'f2' => $id."_2.".$ext['1'],
                    'f3' => $id."_3.".$ext['2']
                );

                $this->db->where('id_livro', $id);

                if($this->db->update('liv_livros', $dados)){
                    return ['status' => 1, 'id' => $id];
                }else{
                    return ['status' => 3, 'id' => $id];
                }

            }else{
                return ['status' => 3, 'id' => $id];
            }

        }

        public function lista_todos() //retorna todos livros
        {
            $this->db->join('liv_autores as a' ,'on liv.id_autor = a.id_autor');
            $this->db->join('liv_editoras as e' ,'on e.id_editora = liv.id_editora');
            return $this->db->get('liv_livros as liv')->result();
        }

        public function lista_detalhes($codigo) //detalhes de livro escolhido
        {
            $this->db->join('liv_autores as a' ,'on liv.id_autor = a.id_autor');
            $this->db->join('liv_editoras as e' ,'on e.id_editora = liv.id_editora');
            $this->db->where('id_livro',$codigo);
            return $this->db->get('liv_livros as liv')->row();
        }

        public function altera_livro()
        {
            $id = $this->input->post('codigo');
            $this->db->where('titulo',$this->input->post('nome'));
            $this->db->where('id_livro !=',$id);
            $ja_tem = $this->db->count_all_results('liv_livros');

            //verifica se nome ja estiver cadastrado e não for esse mesmo
            if($ja_tem > 0){
                return ['status' => 2];
            }

            $dados = array(
               'titulo' => $this->input->post('titulo'),
               'id_editora' => $this->input->post('editora'),
               'id_autor' => $this->input->post('autor'),
               'capa' => $this->input->post('capa'),
               'valor' => $this->input->post('valor'),
            );

            $this->db->where('id_livro', $id);
            if($this->db->update('liv_livros', $dados)){
                
                //essa parte é necessaria para renomear os arquivos, facilitando
                //na hora de trocar uma foto por outra com o unlink, pois pode ser
                //que apenas uma das fotos seja alterada.
                foreach($_FILES as $f){
                    if(isset($f['name'])){
                        $partes = explode('.',$f['name']);
                        $ext[] = end($partes); //pois pode ter mais de 1 "." no nome
                    }
                }                                
                
                //para nao apagar o nome do arquivo que não foi trocado
                //fiz esse foreach                
                foreach($ext as $ponteiro => $e){
                    if(!empty($e)){
                        $apenas_enviadas['f'.($ponteiro+1)] = $id."_".($ponteiro+1).".".$e;
                    }
                }   
                
                if(empty($apenas_enviadas)){ //se nao alterou nenhuma imagem
                    return ['status' => 1, 'id' => $id];
                }

                $this->db->where('id_livro', $id);
                if($this->db->update('liv_livros', $apenas_enviadas)){
                    return ['status' => 1, 'id' => $id];
                }else{
                    return ['status' => 4, 'id' => $id];
                }

                
            }else{
                return ['status' => 3];
            }
        }


        //usado pelo lista_livro.php... ainda não apaga os arquivos
        public function apaga_livro()
        {
            $this->db->where('id_livro', $this->input->post('codigo'));
            return $this->db->delete('liv_livros');
        }


        //usado pelo detalhes_livro.php
        public function exclui_nome_foto()
        {
            $ft = $this->input->post('ft');
            $id = $this->input->post('id');
            
            if(empty($ft)){
                return true;
            }else{
                $numero_img = explode('_',$ft);
                $num = $numero_img[1][0];
                //echo $num;
                //exit;
                $dados = array(
                'f'.$num => null
                );
                
                $this->db->where('id_livro', $id);
                return $this->db->update('liv_livros', $dados);
            }

        }

    }

