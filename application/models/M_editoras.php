<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class M_editoras extends CI_Model {

        public function nova_editora() //salva nova editora no banco
        {
            $this->db->where('editora',$this->input->post('nome'));
            $ja_tem = $this->db->count_all_results('liv_editoras');
            //echo $this->db->get_compiled_select('liv_editoras');

            // var_dump($ja_tem);
            // exit();
            if ($ja_tem > 0) {
                return 2;
            }

            $dados = array(
               'editora' => $this->input->post('nome')
            );

            if($this->db->insert('liv_editoras', $dados)){
                return 1;
            }else{
                return 3;
            }

        }

        public function lista_todos() //retorna todos editoras
        {
            return $this->db->get('liv_editoras')->result();
        }
        
        public function lista_detalhes($codigo) //detalhes de editora escolhido
        {
            $this->db->where('id_editora',$codigo);
            return $this->db->get('liv_editoras')->row();
        }

        public function altera_editora()
        {
            $this->db->where('editora',$this->input->post('nome'));             
            $this->db->where('id_editora !=',$this->input->post('codigo'));
            $ja_tem = $this->db->count_all_results('liv_editoras');
            
            //verifica se nome ja estiver cadastrado e não for esse mesmo
            if($ja_tem > 0){                
                return 2;                
            }

            $dados = array(
               'editora' => $this->input->post('nome')
            );
            
            $this->db->where('id_editora', $this->input->post('codigo'));
            return $this->db->update('liv_editoras', $dados);
        }
        
        public function apaga_editora()
        {
            $this->db->where('id_editora', $this->input->post('codigo'));
            return $this->db->delete('liv_editoras');
        }

    }

