
<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class M_usuarios extends CI_Model {

        public function lista_usu(){
            //DESCOMENTAR AQUI QUANDO OUTROS FOREM USAR
            //$this->db->where('nivel <>',2);
            return $this->db->get('liv_usuarios')->result();
        }

        public function novo_usu()
        {
            $this->db->where('email',$this->input->post('email'));
            $ja_tem = $this->db->count_all_results('liv_usuarios');
                        
            
            if ($ja_tem > 0) {
                return 2;
            }

            $dados = array(
               'nome' => $this->input->post('nome'),               
               'email' => $this->input->post('email'),               
               'senha' => password_hash($this->input->post('senha'),PASSWORD_DEFAULT ),
               'nivel_acesso' => $this->input->post('nivel')
            );

            if($this->db->insert('liv_usuarios', $dados)){
                $id = $this->db->insert_id();
                
                $arquivo = $_FILES['img']['name'];
                $separa = explode('.',$arquivo);
                $ext = end($separa);
                $nome = $id.'.'.$ext;
                $data = array('avatar' => $nome);                
                
                $this->db->where('id_usu', $id);                
                // echo $this->db->set($data)->get_compiled_update('liv_usuarios');                
                // exit;
                if($this->db->update('liv_usuarios', $data)){
                    return ['status' => 1,'id' => $id];
                }else{
                    return 0;
                }
            }

        }

        public function detalhes_usu($codigo)
        {
            $this->db->where('id_usu',$codigo);
            return $this->db->get('liv_usuarios')->row();
        }
        
        public function edita_usu()
        {
            $this->db->where('email',$this->input->post('email'));            
            $this->db->where('id_usu <>',$this->input->post('codigo'));
            $ja_tem = $this->db->get('liv_usuarios')->row();
            //echo $this->db->get_compiled_select('liv_usuarios');
            
            // var_dump($ja_tem);
            // exit();
            if ($ja_tem) {
                return 2;
            }

            $dados = array(
               'nome' => $this->input->post('nome'),               
               'email' => $this->input->post('email'),               
               'senha' => password_hash($this->input->post('senha'),PASSWORD_DEFAULT ),
               'nivel_acesso' => $this->input->post('nivel')
            );
            
            $this->db->where('id_usu', $this->input->post('codigo'));
            return $this->db->update('liv_usuarios', $dados);               
        }
        

        public function bloq_usu()
        {
            $dados = array(
               'ativo' => 0
            );

            $this->db->where('id_usu',  $this->input->post('codigo'));
            return $this->db->update('liv_usuarios', $dados);
        }

        public function desbloq_usu()
        {
            $dados = array(
                'ativo' => 1
             );

            $this->db->where('id_usu', $this->input->post('codigo'));
            return $this->db->update('liv_usuarios', $dados);
        }

        public function apaga_usu()
        {
            $this->db->join('xy_posts as p' ,'on u.id_usu = p.id_usu ');
            $this->db->where('p.id_usu',$this->input->post('codigo'));
            $tem_posts = $this->db->get('liv_usuarios as u')->row();
            
            if (!empty($tem_posts->id_usu)) {
                return 2;
            }
            
            $this->db->where('id_usu', $this->input->post('codigo'));
            return $this->db->delete('liv_usuarios');
        }

    }