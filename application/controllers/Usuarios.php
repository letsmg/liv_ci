<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    public function __construct(){
        parent::__construct();
        
        // if(!$this->session->userdata('codigo')){
        //     redirect(base_url());
        // }
    }

    public function index()
    {
        $this->load->model('m_usuarios');
        $dados['usuarios'] = $this->m_usuarios->lista_usu();

        $this->load->view('principal/cabecalho.php');
        $this->load->view('menus/menu_admin.php');
        $this->load->view('usuarios/lista_usu.php',$dados);
        $this->load->view('principal/rodape.php');
    }


    public function conta_usuarios()
    {
        return $this->db->count('usuarios')->row();
    }



    public function novo(){
        $this->load->view('principal/cabecalho.php');
        $this->load->view('menus/menu_index.php');
        $this->load->view('usuarios/cadastro.php');
        $this->load->view('principal/rodape.php');
    }

    //desativar online
    // public function cad_adminxyz(){
    //     $this->load->model('m_usuarios');
    //     $cadastrado['usuario'] = $this->m_usuarios->conta_usuarios();

    //     $this->load->view('principal/cabecalho.php');
    //     $this->load->view('menus/menu_main.php');
    //     $this->load->view('usuarios/cad_admin.php',$cadastrado);
    //     $this->load->view('principal/rodape.php');

    // }

    public function cadastra()
    {
        $this->load->model('m_usuarios');
        $retorno = $this->m_usuarios->novo_usu();
        // var_dump($_FILES);
        // exit();

        if ($retorno['status'] == 1) {
            $msg = "<div class='alert alert-success'>Cadastrado com sucesso.</div>";

            if($_FILES['img']['name']){
                //var_dump($img);
                //exit();
                $img = $this->faz_upload($retorno['id']);
            }
        }else if ($retorno['status'] == 2) {
            $msg = "<div class='alert alert-danger'>E-mail já cadastrado</div>";
        } else {
            $msg = "<div class='alert alert-danger'>Erro ao cadastrar, por favor tente novamente.</div>";
        }

        $ret = [
            'msg' => $msg,
            'img' => $img,
            'csrf' => $this->security->get_csrf_hash()
        ];

        echo json_encode($ret);
    }

    public function faz_upload($id = null)
    //id é usado quando trocando arquivo por outro e setando id do registro no nome
    {
        $f = $_FILES['img'];
        $nome_original = $f['name'];

        if(!empty($nome_original)){

            if(!empty($id)){
                $codigo = $id;
            }else{
                $codigo = $this->input->post('codigo');
            }
            $dir = './img/usuarios/';
            
            if(!is_dir($dir)){
                mkdir($dir);
            }

            $partes = explode('.',$nome_original);
            $ultima_parte = end($partes); //pois pode ter mais de 1 "." no nome
            $nome = $codigo.'.'.$ultima_parte;
            $tmp = $f['tmp_name'];
            $size = $f['size'];

            $arquivo = $dir.$codigo.".*";

            if (glob($arquivo)) {
                unlink($arquivo);
            }

            if($size > 2000000){
                $img = "<div class='alert alert-danger'>Arquivo ".$nome.' é maior que 2 megas.<br>Pedimos que escolha apenas um avatar simples.</div>';
            }else{
            // if ($this->security->xss_clean($f, TRUE) === true)
            // {
                if(move_uploaded_file($tmp,$dir.$nome)) {                    
                    $img = "<div class='alert alert-success'>Arquivo ".$nome.' foi alterado com sucesso.</div>';
                }else{
                    $img = "<div class='alert alert-success'>Arquivo ".$nome.' não foi enviado.</div>';
                }
            // }else{
            //     $img .= "<div class='alert alert-danger'>Arquivo ".$nome_original.' potencialmente indesejado, por favor envie outro.</div>';
            // }
            }
        }
        return $img;
    }


    public function edicao($codigo)
    {
        $this->load->model('m_usuarios');
        $dados['detalhes'] = $this->m_usuarios->detalhes_usu($codigo);
        // var_dump($dados['detalhes']);
        // exit();
        $this->load->view('principal/cabecalho.php');
        $this->load->view('menus/menu_admin.php');
        $this->load->view('usuarios/detalhes_usu.php',$dados);
        $this->load->view('principal/rodape.php');
    }


    public function edita()
    {
        $this->load->model('m_usuarios');
        $retorno = $this->m_usuarios->edita_usu();

        if ($retorno == 1) {
            $msg = "<div class='alert alert-success'>Dados alterados com sucesso.</div>";
        }else if ($retorno == 2) {
            $msg = "<div class='alert alert-danger'>E-mail já pertence a outro usuário</div>";
        } else {
            $msg = "<div class='alert alert-danger'>Erro ao cadastrar, por favor tente novamente.</div>";
        }

        $ret = [
            'msg' => $msg,
            'csrf' => $this->security->get_csrf_hash()
        ];

        echo json_encode($ret);
    }


    public function bloq_usu()
    {
        $this->load->model('m_usuarios');
        $exec = $this->m_usuarios->bloq_usu();

        if ($exec == 1){
            $msg = "<div class='col-12 alert alert-success'>Usuário bloqueado com sucesso.</div>";
        }else{
            $msg = "<div class='col-12 alert alert-danger'>Erro ao bloquear usuário.</div>";
        }

        $retorno = ['csrf' => $this->security->get_csrf_hash(),
                    'msg' => $msg
                    ];
        $filtro = $this->security->xss_clean($retorno);
        echo json_encode($filtro);
    }

    public function desbloq_usu()
    {
        $this->load->model('m_usuarios');
        $exec = $this->m_usuarios->desbloq_usu();

        if ($exec == 1){
            $msg = "<div class='col-12 alert alert-success'>Usuário desbloqueado com sucesso.</div>";
        }else{
            $msg = "<div class='col-12 alert alert-danger'>Erro ao desbloquear usuário.</div>";
        }

        $retorno = ['csrf' => $this->security->get_csrf_hash(),
                    'msg' => $msg
                    ];
        $filtro = $this->security->xss_clean($retorno);
        echo json_encode($filtro);
    }


    public function excluir()
    {
        $this->load->model('m_usuarios');
        $apagou = $this->m_usuarios->apaga_usu();
        $extra = "";
        if ($apagou == 1) {
            $msg = "<div class='col-12 alert alert-success'>Usuário apagado com sucesso.</div>";
        } else if ($apagou == 2) {
            $msg = "<div class='col-12 alert alert-danger'>Existem posts desse usuário, não pode ser apagado.</div>";
            $extra = '2';
        }else{
            $msg = "<div class='col-12 alert alert-success'>Erro ao apagar usuário.</div>";
        }

        $retorno = ['csrf' => $this->security->get_csrf_hash(),
                    'msg' => $msg,
                    'extra' => $extra
                    ];
        $filtro = $this->security->xss_clean($retorno);
        echo json_encode($filtro);
    }




}