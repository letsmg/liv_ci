<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Principal extends CI_Controller {

        public function __construct(){
            parent::__construct();

            //metodo construtor.. sempre vai chamar a model quando acessar a classe
            if(isset($_SESSION['codigo'])){
                redirect('autores/lista');
            }
        }

        public function index(){
            $this->load->model('m_livros');
            $dados['livros'] = $this->m_livros->lista_todos();

            $this->load->view('principal/cabecalho.php');
            $this->load->view('menus/menu_index');
            $this->load->view('principal/index.php',$dados);
            $this->load->view('principal/rodape.php');
        }

               

        
    }