<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Autores extends CI_Controller {

        public function __construct(){
            parent::__construct();
            //metodo construtor.. sempre vai chamar a model quando acessar a classe
            if(!isset($_SESSION['codigo'])){
                redirect('principal');
            }

            $this->load->model('m_autores');
        }

        public function lista(){            
            $dados['autores'] = $this->m_autores->lista_todos();

            $this->load->view('principal/cabecalho.php');
            $this->load->view('menus/menu_admin');
            $this->load->view('autores/lista_autores.php',$dados);
            $this->load->view('principal/rodape.php');
        }

        public function novo(){
            $dados['autores'] = $this->m_autores->lista_todos();

            $this->load->view('principal/cabecalho.php');
            $this->load->view('menus/menu_admin');
            $this->load->view('autores/novo_autor.php',$dados);
            $this->load->view('principal/rodape.php');
        }

        public function cadastra()
        {
            $retorno = $this->m_autores->novo_autor();
            // var_dump($_FILES);
            // exit();
    
            if ($retorno == 1) {
                $msg = "<div class='alert alert-success'>Cadastrado com sucesso.</div>";                    
            }else if ($retorno == 2) {
                $msg = "<div class='alert alert-danger'>Autor já cadastrado.</div>";
            } else {
                $msg = "<div class='alert alert-danger'>Erro ao cadastrar, por favor tente novamente.</div>";
            }
    
            $ret = [
                'msg' => $msg,
                'csrf' => $this->security->get_csrf_hash()
            ];
    
            echo json_encode($ret);
        }

        public function edicao($codigo)
        {
            $dados['detalhes'] = $this->m_autores->lista_detalhes($codigo);
            // var_dump($dados['detalhes']);
            // exit();
            $this->load->view('principal/cabecalho.php');
            $this->load->view('menus/menu_admin');
            $this->load->view('autores/detalhes_autor.php',$dados);
            $this->load->view('principal/rodape.php');
        }

        public function edita()
        {
            $editado = $this->m_autores->altera_autor();
            
            if ($editado === 2) {
                $msg = "<div class='col-12 alert alert-danger'>Autor já cadastrado.</div>";
            } else if ($editado == 1) {
                $msg = "<div class='col-12 alert alert-success'>Dados alterados com sucesso.</div>";                
            }else{
                $msg = "<div class='col-12 alert alert-danger'>Erro ao alterar dados.</div>";
            }

            $retorno = ['csrf' => $this->security->get_csrf_hash(),
                        'msg' => $msg,                    
                       ];

            $filtro = $this->security->xss_clean($retorno); //proteje contra ataque xss
            echo json_encode($filtro);
        }
        
        public function excluir()
        {
            $apagou = $this->m_autores->apaga_autor();

            if ($apagou) {
                $msg = "<div class='col-12 alert alert-success'>Autor apagado com sucesso.</div>";
            } else {
                $msg = "<div class='col-12 alert alert-success'>Erro ao apagar autor.</div>";
            }

            $retorno = ['csrf' => $this->security->get_csrf_hash(),
                        'msg' => $msg
                        ];
            $filtro = $this->security->xss_clean($retorno);
            echo json_encode($filtro);
        }        

        
    }