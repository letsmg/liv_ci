<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class livros extends CI_Controller {

        public function __construct(){
            parent::__construct();
            //metodo construtor.. sempre vai chamar a model quando acessar a classe
            if(!isset($_SESSION['codigo'])){
                redirect('principal');
            }

            $this->load->model('m_livros');
        }

        public function lista(){
            $dados['livros'] = $this->m_livros->lista_todos();

            $this->load->view('principal/cabecalho.php');
            $this->load->view('menus/menu_admin');
            $this->load->view('livros/lista_livros.php',$dados);
            $this->load->view('principal/rodape.php');
        }

        public function novo(){
            $dados['livros'] = $this->m_livros->lista_todos();

            $this->load->model('m_editoras');
            $dados['editoras'] = $this->m_editoras->lista_todos();

            $this->load->model('m_autores');
            $dados['autores'] = $this->m_autores->lista_todos();

            $this->load->view('principal/cabecalho.php');
            $this->load->view('menus/menu_admin');
            $this->load->view('livros/novo_livro.php',$dados);
            $this->load->view('principal/rodape.php');
        }

        public function cadastra()
        {
            $retorno = $this->m_livros->novo_livro();
            $extra ="";
            $img = "";
            if ($retorno['status'] == 1) {
                $msg = "<div class='alert alert-success'>Cadastrado com sucesso.</div>";
                //só inclui as imagens se o insert no banco dar certo
                if($_FILES['img']['name']  || $_FILES['img2']['name'] || $_FILES['img3']['name']){
                    //var_dump($img);
                    //exit();
                    $img = $this->faz_upload($retorno['id']);
                }
            }else if ($retorno['status'] == 2) {
                $msg = "<div class='alert alert-danger'>livro já cadastrado.</div>";
            } else {
                $msg = "<div class='alert alert-danger'>Erro ao cadastrar, por favor tente novamente.</div>";
            }

            $ret = ['csrf' => $this->security->get_csrf_hash(),
                    'msg' => $msg,
                    'img' => $img,
                    'extra' => $retorno
                    ];

            echo json_encode($ret);
        }

        public function faz_upload($id = null)
        //id é usado quando trocando arquivo por outro e setando id do registro no nome
        {
            $img = ''; //setado vazio para concatenar no retorno
            $i=1; //setado 1 para individualizar fotos
            foreach($_FILES as $f){
                $nome_arquivo_da_vez = $f['name'];

                if(!empty($nome_arquivo_da_vez)){

                    if(!empty($id)){ //se nao esta vazio, quer dize que foi cadastrado agora
                        $codigo = $id;
                    }else{ //caso contrário, está alterando dados, então o codigo vem pelo post
                        $codigo = $this->input->post('codigo');
                    }
                    $dir = './img/livros/'.$codigo.'/';

                    if(!is_dir($dir)){
                        mkdir($dir);
                    }

                    $partes = explode('.',$nome_arquivo_da_vez);
                    $ultima_parte = end($partes); //pois pode ter mais de 1 "." no nome "ferias.verao.alegria.jpg"
                    $nome = $codigo."_".$i.'.'.$ultima_parte;
                    $tmp = $f['tmp_name'];
                    $size = $f['size'];
                    
                    foreach (glob($dir.$codigo."_".$i.".*") as $arquivo) {
                        unlink($arquivo);
                    }
                    
                    // if ($this->security->xss_clean($f, TRUE) === true)
                    // {
                        if(move_uploaded_file($tmp,$dir.$nome)) {
                            $img .= "<div class='alert alert-success'>Arquivo ".$nome.' foi alterado com sucesso.</div>';
                        }else{
                            $img .= "<div class='alert alert-success'>Arquivo ".$nome.' não foi enviado.</div>';
                        }
                    // }else{
                    //     $img .= "<div class='alert alert-danger'>Arquivo ".$nome_arquivo_da_vez.' potencialmente indesejado, por favor envie outro.</div>';
                    // }
                }
                $i++;
            }
            return $img;
        }

        public function edicao($codigo)
        {
            $dados['detalhes'] = $this->m_livros->lista_detalhes($codigo);
            
            $this->load->model('m_editoras');
            $dados['editoras'] = $this->m_editoras->lista_todos();

            $this->load->model('m_autores');
            $dados['autores'] = $this->m_autores->lista_todos();

            $this->load->view('principal/cabecalho.php');
            $this->load->view('menus/menu_admin');
            $this->load->view('livros/detalhes_livro.php',$dados);
            $this->load->view('principal/rodape.php');
        }

        public function edita()
        {
            $ret_edicao = $this->m_livros->altera_livro();
            $img = "";
            if ($ret_edicao['status'] === 2) {
                $msg = "<div class='col-12 alert alert-danger'>Livro já cadastrado.</div>";
            } else if ($ret_edicao['status'] == 1) {
                $msg = "<div class='col-12 alert alert-success'>Dados alterados com sucesso.</div>";

                //só inclui as imagens se o insert no banco dar certo
                if($_FILES['img']['name']  || $_FILES['img2']['name'] || $_FILES['img3']['name']){
                    //var_dump($img);
                    //exit();
                    $img = $this->faz_upload($ret_edicao['id']);
                }
            }else if ($ret_edicao['status'] == 4) {
                $msg = "<div class='col-12 alert alert-success'>Dados alterados, mas erro ao alterar nome das fotos no banco.</div>";                
            }else{
                $msg = "<div class='col-12 alert alert-danger'>Erro ao alterar dados.</div>";
            }
            
            $retorno = ['csrf' => $this->security->get_csrf_hash(),
                    'msg' => $msg,
                    'img' => $img,
                    'extra' => $ret_edicao
                    ];
        
            $filtro = $this->security->xss_clean($retorno); //proteje contra ataque xss
            echo json_encode($filtro);
        }

        //usado pelo lista_livros.php
        public function excluir()
        {
            $apagou = $this->m_livros->apaga_livro();

            if ($apagou) {
                $msg = "<div class='col-12 alert alert-success'>livro apagado com sucesso.</div>";
            } else {
                $msg = "<div class='col-12 alert alert-success'>Erro ao apagar livro.</div>";
            }

            $retorno = ['csrf' => $this->security->get_csrf_hash(),
                        'msg' => $msg
                        ];
            $filtro = $this->security->xss_clean($retorno);
            echo json_encode($filtro);
        }


        //usado pelo detalhes_livro.php apaga arquivos também
        public function apagaft(){        
            $excluiu = $this->m_livros->exclui_nome_foto();

            if($excluiu){
                $ft = $this->input->post('ft');
                $id = $this->input->post('id');
                $arquivo = 'img/posts/p/'.$id.'/'.$ft;            
                
                if(is_file($arquivo)){
                    unlink($arquivo);
                    $msg = "<div class='alert alert-success'>Imagem ".$ft." apagada com sucesso</div>";                 
                }else{
                    $msg = "<div class='alert alert-danger'>Imagem inexistente</div>";                 
                }
                
                $retorno = ['csrf' => $this->security->get_csrf_hash(),
                            'msg' => $msg
                            ];
                $filtro = $this->security->xss_clean($retorno);
                echo json_encode($filtro);
            }
        }


    }