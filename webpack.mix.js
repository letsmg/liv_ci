const mix = require('laravel-mix');

mix//.js('resources/js/app.js', 'public/js')
   .copy('node_modules/jquery/dist/jquery.min.js', 'js/jquery.min.js')
   .copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'css/bootstrap.min.css')
   .copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'js/bootstrap.min.js')
   .copy('node_modules/font-awesome/css/font-awesome.min.css', 'css/font-awesome.min.css')
   .copy('node_modules/font-awesome/fonts/', 'fonts/')   
   .sass('scss/geral/diversos.scss', 'css/diversos.css')
   .sass('scss/geral/margens.scss', 'css/margens.css')
   .sass('scss/geral/mediaqueries.scss', 'css/mediaqueries.css')
   .sass('scss/geral/backgrounds.scss', 'css/backgrounds.css')
   .sass('scss/geral/cores.scss', 'css/cores.css')
   .sass('scss/geral/fontes.scss', 'css/fontes.css')
   ;